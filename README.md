![Logo](http://www.tisv.cn/img/logo.png)

--------------------------------------------------------------------------------


[![Build Status](http://www.tisv.cn/img/badge.svg)](http://www.tisv.cn/) [![GitHub stars](http://www.tisv.cn/img/givemeastar.png)](https://github.com/AITutorials/)


### 使命

我们致力于教育行业，试图打造最优质的人工智能学习社区，出品最专业并最受欢迎的人工智能教程!

---

### 致谢

感谢以下机构或站点提供的人工智能工具和参考学习资料。


[![avatar](http://www.tisv.cn/img/lockup.svg)](https://tensorflow.google.cn/)
[![avatar](http://www.tisv.cn/img/Pytorch_logo-1024x205.png)](https://pytorch.org/)


### 合作

邮箱: os@orangestar.vip

---


## 其他资源

* [Dataset(集成世界范围内重要AI技术解决方案)](https://github.com/AITutorials/dataset)

* [Solutions(集成世界范围内重要AI技术解决方案)](https://github.com/AITutorials/solutions)

* [Examples(展示AI实战教程中的示例)](https://github.com/AITutorials/examples)

---
